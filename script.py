import psycopg2

input="./records.txt"

try:
    with open(input, 'w') as f:
        f.write("o.muhametaj@teamsystem.com")
        f.flush()
    print("Data written to the file successfully.")
except FileNotFoundError:
    print("File not found. Please check the file path.")
except PermissionError:
    print("Permission denied. Cannot write to the file.")
except Exception as e:
    print("An error occurred:", e)

